let _globalAjaxUrl = '';
let _ajaxUrlPath = '';
let _apiVersion = '';

$(document).ready (function (){
    _globalAjaxUrl = 'http://localhost:8080/';
    _apiVersion = 'api/v1';
});

$("#userTbleBtnl").click(function() {
    loadUserTable ();
});

function loadUserTable (){

  _ajaxUrlPath = '/listUsers'

  $('#userTbl').DataTable( {
      ajax: {
          url: _globalAjaxUrl +_apiVersion +_ajaxUrlPath,
          dataSrc: "",
          mDataProp: "",
          "columns": [{
              "data": "firstName",
              "data": "lastName",
              "data": "phoneNumber",
              "data": "email",
              "data": "registrationNumber",
              "data": "addressCountry",
              "data": "addressLine"
          }],
          autofill: true,
          order: [[0, 'asc']],
          select: true,
          responsive: true,
          buttons: true,
          length: 10,
          dom: 'Bfrtip',
          buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
          ]
      }
  });

}




