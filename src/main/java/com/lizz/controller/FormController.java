package com.lizz.controller;

import com.lizz.model.Planner;
import com.lizz.model.Bid;
import com.lizz.utils.RegistrationUIInput;
import com.lizz.service.bid.BidService;
import com.lizz.service.planner.PlannerService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.concurrent.atomic.AtomicInteger;

@Controller
public class FormController {

    protected Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private PlannerService plannerService;

    @Autowired
    private BidService bidService;

    private AtomicInteger c = new AtomicInteger(0);

    @PostMapping("/register")
    public String register (@ModelAttribute RegistrationUIInput formInput, Model model){

         String msg = "Planner registration successful, an email has been sent to you with further instructions";

        logger.info(formInput.toString());
        //plannerService.add(planner);

        model.addAttribute("msg", msg);
        model.addAttribute("planner", new Planner());

        return "/registration";

    }





}
