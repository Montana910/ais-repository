package com.lizz.controller;

import com.lizz.model.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DefaultController {

    @GetMapping("/")
    public String home1() {
        return "home";
    }

    @GetMapping("/home")
    public String home() {
        return "home";
    }

    @GetMapping("/admin")
    public String admin() {
        return "/admin";
    }

    @GetMapping("/bid")
    public String bid() {

        return "bid";
    }

    @GetMapping("/loggedIn-home")
    public String login_home (){

        return "login-home";

    }

    @GetMapping("/supplier")
    public String supplier() {

        return "supplier-portal";
    }

    @GetMapping("/bid-board")
    public String bidBoard() {

        return "bid-board";
    }

    @GetMapping("/about")
    public String about() {
        return "/about";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("user", new Planner());
        return "registration";
    }

    @GetMapping("/planner")
    public String planner(Model model) {
        model.addAttribute("ad", new Bid());
        return "planner";
    }

    @GetMapping("/designer")
    public String designer(Model model) {
        model.addAttribute("ad", new Bid());
        return "/planner";
    }

    @RequestMapping("/403")
    public String error403() {
        return "/error/403";
    }

    // Login form with error
    @RequestMapping("/login-error.html")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login.html";
    }

}
