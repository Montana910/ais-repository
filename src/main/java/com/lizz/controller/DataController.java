package com.lizz.controller;

import com.lizz.model.Planner;
import com.lizz.service.planner.PlannerService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class DataController {

    protected Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private PlannerService plannerService;

    @GetMapping("/listUsers")
    public List<Planner> getListOfUsers (){
        logger.info("fetching all user from DB");
        return new ArrayList<>();
    }
}
