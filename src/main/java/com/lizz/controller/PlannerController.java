package com.lizz.controller;

import com.lizz.model.Bid;
import com.lizz.model.Planner;
import com.lizz.model.User;
import com.lizz.service.bid.BidService;
import com.lizz.service.planner.PlannerService;
import com.lizz.service.user.IUserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PlannerController {

    private Log logger = LogFactory.getLog(PlannerController.class);

    @Autowired
    private PlannerService plannerService;

    @Autowired
    private BidService bidService;

    @Autowired
    private IUserService iUserService;

    /**
     * Planner post a bid on the platform,
     * @param bid bid information/details
     * @param model Bid form object
     * @return http status
     * @throws Exception
     */
    @PostMapping("/postAd")
    public @ResponseBody
    String postAd (@ModelAttribute Bid bid, Model model) throws Exception {



            Planner getLoggedInPlanner = getLoggedInPlanner ();

            // bid.setServiceProviderBid(new );
            logger.info("Ajax post repository {}" + bid.toString());
            bid.setPlanner(getLoggedInPlanner);
            bidService.add(bid);

//            String msg = "Bid Advertised successful, confirmation email send";
//            JSONObject jsonResponse = new JSONObject("{'msg':'"+msg+"', 'msgCode': '200'}");
//            return jsonResponse.toString();
       return "Working";

    }

    public Planner getLoggedInPlanner () throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User theUser = iUserService.findByUserName(currentPrincipalName);

        return plannerService.findByUserName(theUser.getFirstName(), theUser.getLastName(), theUser.getEmail());
    }




}