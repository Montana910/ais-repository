package com.lizz.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lizz.model.Bid;
import com.lizz.service.bid.BidService;
import com.lizz.service.planner.PlannerService;
import com.lizz.utils.BidContainer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Controller
public class BidController {

    private Log logger = LogFactory.getLog(PlannerController.class);

    @Autowired
    private PlannerService plannerService;

    @Autowired
    private BidService bidService;

    @GetMapping(value= "/listBids", produces = "application/json")
    public @ResponseBody List<BidContainer> getAllBids () throws JsonProcessingException {

        List<BidContainer> getAllBids = new ArrayList<>();
        List<Bid> bidList = bidService.get();
        bidList.forEach((x) -> {
            BidContainer container = getBidWithOutPlanner(x);
            logger.info("Bid Container constructed [{}]");
            getAllBids.add(container);
        });
        return getAllBids;
    }

    public BidContainer getBidWithOutPlanner (Bid bid){
        BidContainer tempContainer = new BidContainer(bid.getId(), bid.getBidTitle(), bid.getBidDescription(), bid.getBidOpenDate(), bid.getBidCloseDate(), bid.getBidHits());
        return tempContainer;
    }


}
