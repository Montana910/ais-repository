package com.lizz.controller;

import com.lizz.model.*;
import com.lizz.model.forms.BuyerFormInputs;
import com.lizz.model.forms.ManufactureFormInputs;
import com.lizz.model.forms.SellerFormInputs;
import com.lizz.service.bid.BidService;
import com.lizz.service.planner.PlannerService;
import com.lizz.service.role.IRoleService;
import com.lizz.service.serviceprovider.ServiceProviderService;
import com.lizz.service.user.IUserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;

@Controller
public class CustomerRegistrationController {

    private Log logger = LogFactory.getLog(PlannerController.class);

    @Autowired
    private PlannerService plannerService;

    @Autowired
    private BidService bidService;

    @Autowired
    private ServiceProviderService spService;


    @Autowired
    private IUserService userService;

    @Autowired
    private IRoleService roleService;

    /**
     * The steel buyer in this sense is the planner that likely works for a high-end steel production company
     * where his duties could include steel raw material procurement (Planner).
     * @param buyerFromInputs
     * @param model
     * @return
     */
    @PostMapping("/register-buyer")
    public String registerBuyer (@ModelAttribute BuyerFormInputs buyerFromInputs, Model model){

        String msg = "";

        logger.info("New buyer registration request, with repository {}" + buyerFromInputs.toString());
        Planner newPlanner = new Planner();
        newPlanner.setpCompanyName(buyerFromInputs.getBuyer_company_name());
        newPlanner.setpWorkEmail(buyerFromInputs.getBuyer_company_email());
        newPlanner.setpMrkSector(buyerFromInputs.getBuyer_company_type());
        newPlanner.setpIndustry(buyerFromInputs.getBuyer_company_identification_type());
        plannerService.add(newPlanner);

        logger.info("Trying to create role for profile {}" + buyerFromInputs.getBuyer_company_email());
        Role profileRole = new Role();
        profileRole.setName("ROLE_USER");
        roleService.add(profileRole);

//        profileRole.setPrivileges(new ArrayList<>());
//        profileRole.setUsers(new ArrayList<>());

        logger.info("Trying to create user profile for planner {}" + buyerFromInputs.getBuyer_company_email());
        User profile = new User();
        profile.setFirstName(buyerFromInputs.getBuyer_company_name());
        profile.setLastName(buyerFromInputs.getBuyer_company_name());
        profile.setEmail(buyerFromInputs.getBuyer_company_email());
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(buyerFromInputs.getConfirmed_password());
        profile.setPassword(encodedPassword);

        profile.setEnabled(false);
        profile.setTokenExpired(true);
        ArrayList<Role> roles = new ArrayList();
        roles.add(profileRole);
        profile.setRoles(roles);
        userService.add(profile);

        model.addAttribute("msg", msg);
        model.addAttribute("email", buyerFromInputs.getBuyer_company_email());

        return "supplier-portal";

    }

    /**
     * The steel Seller in this sense is the Service Provider that could be able to deliver on the planner's high demand.
     * where his duties could include steel raw material procurement (Planner).
     * @param sellerFromInputs
     * @param model
     * @return
     */
    @PostMapping("/register-seller")
    public String registerSeller (@ModelAttribute SellerFormInputs sellerFromInputs, Model model){

        String msg = "";

        logger.info("New seller registration request, with repository {} "+ sellerFromInputs.toString());
        ServiceProvider sp = new ServiceProvider();
        sp.setSpCompanyName(sellerFromInputs.getSeller_company_name());
        sp.setSpEmail(sellerFromInputs.getSeller_company_email());
        //sp.setBids(new ArrayList<Bid>);
        spService.add(sp);

        model.addAttribute("msg", msg);
        model.addAttribute("email", sellerFromInputs.getSeller_company_email());
        //model.addAttribute("planner", new Planner());

        return "supplier-portal";

    }


    @PostMapping("/register-manufacture")
    public String registerManufacture (@ModelAttribute ManufactureFormInputs manufactureFromInputs, Model model){

        String msg = "";

        logger.info("New manufacture request, with repository {}" + manufactureFromInputs.toString());


        model.addAttribute("msg", msg);
        model.addAttribute("email", manufactureFromInputs.getManufacture_company_email());


        return "supplier-portal";

    }
}
