package com.lizz.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ServiceProvider {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String spName;
    private String spSurname;
    private String spIndustry;
    private String spSector;
    private String spCompanyName;
    private String spWebSite;
    private String spEmail;
    private String spAddress;
    private String spTell;
    private String spContact;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(joinColumns = @JoinColumn(name = "Service_provider_id"),
            inverseJoinColumns = @JoinColumn(name = "bid_id"))
    private Set<Bid> bids = new HashSet<>();

    public ServiceProvider( String spName, String spSurname, String spIndustry, String spSector, String spCompanyName, String spWebSite, String spEmail, String spAddress, String spTell, String spContact) {
        this.spName = spName;
        this.spSurname = spSurname;
        this.spIndustry = spIndustry;
        this.spSector = spSector;
        this.spCompanyName = spCompanyName;
        this.spWebSite = spWebSite;
        this.spEmail = spEmail;
        this.spAddress = spAddress;
        this.spTell = spTell;
        this.spContact = spContact;
    }

    public ServiceProvider() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long Id) {
        this.id = Id;
    }

    public String getSpName() {
        return spName;
    }

    public void setSpName(String spName) {
        this.spName = spName;
    }

    public String getSpSurname() {
        return spSurname;
    }

    public void setSpSurname(String spSurname) {
        this.spSurname = spSurname;
    }

    public String getSpIndustry() {
        return spIndustry;
    }

    public void setSpIndustry(String spIndustry) {
        this.spIndustry = spIndustry;
    }

    public String getSpSector() {
        return spSector;
    }

    public void setSpSector(String spSector) {
        this.spSector = spSector;
    }

    public String getSpCompanyName() {
        return spCompanyName;
    }

    public void setSpCompanyName(String spCompanyName) {
        this.spCompanyName = spCompanyName;
    }

    public String getSpWebSite() {
        return spWebSite;
    }

    public void setSpWebSite(String spWebSite) {
        this.spWebSite = spWebSite;
    }

    public String getSpEmail() {
        return spEmail;
    }

    public void setSpEmail(String spEmail) {
        this.spEmail = spEmail;
    }

    public String getSpAddress() {
        return spAddress;
    }

    public void setSpAddress(String spAddress) {
        this.spAddress = spAddress;
    }

    public String getSpTell() {
        return spTell;
    }

    public void setSpTell(String spTell) {
        this.spTell = spTell;
    }

    public String getSpContact() {
        return spContact;
    }

    public Set<Bid> getBids() {
        return bids;
    }

    public void setBids(Set<Bid> bids) {
        this.bids = bids;
    }

    public void setSpContact(String spContact) {
        this.spContact = spContact;
    }

    @Override
    public String toString() {
        return "ServiceProvider{" +
                "spUserId=" + id +
                ", spName='" + spName + '\'' +
                ", spSurname='" + spSurname + '\'' +
                ", spIndustry='" + spIndustry + '\'' +
                ", spSector='" + spSector + '\'' +
                ", spCompanyName='" + spCompanyName + '\'' +
                ", spWebSite='" + spWebSite + '\'' +
                ", spEmail='" + spEmail + '\'' +
                ", spAddress='" + spAddress + '\'' +
                ", spTell='" + spTell + '\'' +
                ", spContact='" + spContact + '\'' +
                '}';
    }
}
