package com.lizz.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class Planner {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String pCompanyName;
    private String pIndustry;
    private String pMarketSector;
    private String pMrkSector;
    private String pName;
    private String pPosition;
    private String pSurname;
    private String pWorkAddress;
    private String pWorkContact;
    private String pWorkEmail;
    private String pWorkTell;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long pUserId) {
        this.id = pUserId;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpSurname() {
        return pSurname;
    }

    public void setpSurname(String pSurname) {
        this.pSurname = pSurname;
    }

    public String getpIndustry() {
        return pIndustry;
    }

    public void setpIndustry(String pIndustry) {
        this.pIndustry = pIndustry;
    }

    public String getpMrkSector() {
        return pMrkSector;
    }

    public void setpMrkSector(String pMrkSector) {
        this.pMrkSector = pMrkSector;
    }

    public String getpCompanyName() {
        return pCompanyName;
    }

    public void setpCompanyName(String pCompanyName) {
        this.pCompanyName = pCompanyName;
    }

    public String getpMarketSector() {
        return pMarketSector;
    }

    public void setpMarketSector(String pMarketSector) {
        this.pMarketSector = pMarketSector;
    }

    public String getpPosition() {
        return pPosition;
    }

    public void setpPosition(String pPosition) {
        this.pPosition = pPosition;
    }

    public String getpWorkEmail() {
        return pWorkEmail;
    }

    public void setpWorkEmail(String pWorkEmail) {
        this.pWorkEmail = pWorkEmail;
    }

    public String getpWorkAddress() {
        return pWorkAddress;
    }

    public void setpWorkAddress(String pWorkAddress) {
        this.pWorkAddress = pWorkAddress;
    }

    public String getpWorkTell() {
        return pWorkTell;
    }

    public void setpWorkTell(String pWorkTell) {
        this.pWorkTell = pWorkTell;
    }

    public String getpWorkContact() {
        return pWorkContact;
    }

    public void setpWorkContact(String pWorkContact) {
        this.pWorkContact = pWorkContact;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public Planner(Long  id, String pCompanyName, String pIndustry, String pMarketSector, String pMrkSector, String pName, String pPosition, String pSurname, String pWorkAddress, String pWorkContact, String pWorkEmail, String pWorkTell) {
        this.id = id;
        this.pCompanyName = pCompanyName;
        this.pIndustry = pIndustry;
        this.pMarketSector = pMarketSector;
        this.pMrkSector = pMrkSector;
        this.pName = pName;
        this.pPosition = pPosition;
        this.pSurname = pSurname;
        this.pWorkAddress = pWorkAddress;
        this.pWorkContact = pWorkContact;
        this.pWorkEmail = pWorkEmail;
        this.pWorkTell = pWorkTell;
    }

    public Planner() {

    }

    @Override
    public String toString() {
        return "Planner{" +
                "id=" + id +
                ", pCompanyName='" + pCompanyName + '\'' +
                ", pIndustry='" + pIndustry + '\'' +
                ", pMarketSector='" + pMarketSector + '\'' +
                ", pMrkSector='" + pMrkSector + '\'' +
                ", pName='" + pName + '\'' +
                ", pPosition='" + pPosition + '\'' +
                ", pSurname='" + pSurname + '\'' +
                ", pWorkAddress='" + pWorkAddress + '\'' +
                ", pWorkContact='" + pWorkContact + '\'' +
                ", pWorkEmail='" + pWorkEmail + '\'' +
                ", pWorkTell='" + pWorkTell + '\'' +
                '}';
    }
}
