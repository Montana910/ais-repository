package com.lizz.model;

import com.fasterxml.jackson.annotation.JsonRawValue;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Bid {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @JsonRawValue
    private Long id;

    private String bidTitle;
    @JsonRawValue
    private String bidDescription;
    @JsonRawValue
    private String bidOpenDate;
    @JsonRawValue
    private String bidCloseDate;
    @JsonRawValue
    private int bidHits;

    @ManyToOne
    @JoinColumn(name = "planner_id", nullable = false)
    private Planner planner;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBidTitle() {
        return bidTitle;
    }

    public void setBidTitle(String bidTitle) {
        this.bidTitle = bidTitle;
    }

    public String getBidDescription() {
        return bidDescription;
    }

    public void setBidDescription(String bidDescription) {
        this.bidDescription = bidDescription;
    }

    public String getBidOpenDate() {
        return bidOpenDate;
    }

    public void setBidOpenDate(String bidOpenDate) {
        this.bidOpenDate = bidOpenDate;
    }

    public String getBidCloseDate() {
        return bidCloseDate;
    }

    public void setBidCloseDate(String bidCloseDate) {
        this.bidCloseDate = bidCloseDate;
    }

    public int getBidHits() {
        return bidHits;
    }

    public void setBidHits(int bidHits) {
        this.bidHits = bidHits;
    }

    public Planner getPlanner() {
        if (this.planner == null){
            return new Planner();
        }
        return planner;
    }

    public void setPlanner(Planner planner) {
        this.planner = planner;
    }

    @Override
    public String toString() {
        return "Bid{" +
                "bidId=" + id +
                ", bidTitle='" + bidTitle + '\'' +
                ", bidDescription='" + bidDescription + '\'' +
                ", bidOpenDate='" + bidOpenDate + '\'' +
                ", bidCloseDate='" + bidCloseDate + '\'' +
                ", bidHits=" + bidHits +
                ", planner=" + planner +
                '}';
    }
}