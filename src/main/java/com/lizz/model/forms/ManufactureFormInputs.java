package com.lizz.model.forms;

public class ManufactureFormInputs {

    private String manufacture_company_name;
    private String manufacture_company_type;
    private String manufacture_company_identification_type;
    private String manufacture_company_identification_number;
    private String manufacture_company_email;
    private String manufacture_company_email_password;
    private String confirmed_password;

    public String getManufacture_company_name() {
        return manufacture_company_name;
    }

    public String getManufacture_company_type() {
        return manufacture_company_type;
    }

    public String getManufacture_company_identification_type() {
        return manufacture_company_identification_type;
    }

    public String getManufacture_company_identification_number() {
        return manufacture_company_identification_number;
    }

    public String getManufacture_company_email() {
        return manufacture_company_email;
    }

    public String getManufacture_company_email_password() {
        return manufacture_company_email_password;
    }

    public String getConfirmed_password() {
        return confirmed_password;
    }

    public void setManufacture_company_name(String manufacture_company_name) {
        this.manufacture_company_name = manufacture_company_name;
    }

    public void setManufacture_company_type(String manufacture_company_type) {
        this.manufacture_company_type = manufacture_company_type;
    }

    public void setManufacture_company_identification_type(String manufacture_company_identification_type) {
        this.manufacture_company_identification_type = manufacture_company_identification_type;
    }

    public void setManufacture_company_identification_number(String manufacture_company_identification_number) {
        this.manufacture_company_identification_number = manufacture_company_identification_number;
    }

    public void setManufacture_company_email(String manufacture_company_email) {
        this.manufacture_company_email = manufacture_company_email;
    }

    public void setManufacture_company_email_password(String manufacture_company_email_password) {
        this.manufacture_company_email_password = manufacture_company_email_password;
    }

    public void setConfirmed_password(String confirmed_password) {
        this.confirmed_password = confirmed_password;
    }

    @Override
    public String toString() {
        return "ManufactureFormInputs{" +
                "manufacture_company_name='" + manufacture_company_name + '\'' +
                ", manufacture_company_type='" + manufacture_company_type + '\'' +
                ", manufacture_company_identification_type='" + manufacture_company_identification_type + '\'' +
                ", manufacture_company_identification_number='" + manufacture_company_identification_number + '\'' +
                ", manufacture_company_email='" + manufacture_company_email + '\'' +
                ", manufacture_company_email_password='" + manufacture_company_email_password + '\'' +
                ", confirmed_password='" + confirmed_password + '\'' +
                '}';
    }
}
