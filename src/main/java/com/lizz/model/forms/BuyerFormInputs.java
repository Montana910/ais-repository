package com.lizz.model.forms;

public class BuyerFormInputs {

    private String buyer_company_name;
    private String buyer_company_type;
    private String buyer_company_identification_type;
    private String buyer_company_identification_number;
    private String buyer_company_email;
    private String buyer_company_email_password;
    private String confirmed_password;

    public String getBuyer_company_name() {
        return buyer_company_name;
    }

    public String getBuyer_company_type() {
        return buyer_company_type;
    }

    public String getBuyer_company_identification_type() {
        return buyer_company_identification_type;
    }

    public String getBuyer_company_identification_number() {
        return buyer_company_identification_number;
    }

    public String getBuyer_company_email() {
        return buyer_company_email;
    }

    public String getBuyer_company_email_password() {
        return buyer_company_email_password;
    }

    public String getConfirmed_password() {
        return confirmed_password;
    }

    public void setBuyer_company_name(String buyer_company_name) {
        this.buyer_company_name = buyer_company_name;
    }

    public void setBuyer_company_type(String buyer_company_type) {
        this.buyer_company_type = buyer_company_type;
    }

    public void setBuyer_company_identification_type(String buyer_company_identification_type) {
        this.buyer_company_identification_type = buyer_company_identification_type;
    }

    public void setBuyer_company_identification_number(String buyer_company_identification_number) {
        this.buyer_company_identification_number = buyer_company_identification_number;
    }

    public void setBuyer_company_email(String buyer_company_email) {
        this.buyer_company_email = buyer_company_email;
    }

    public void setBuyer_company_email_password(String buyer_company_email_password) {
        this.buyer_company_email_password = buyer_company_email_password;
    }

    public void setConfirmed_password(String confirmed_password) {
        this.confirmed_password = confirmed_password;
    }

    @Override
    public String toString() {
        return "BuyerFormInputs{" +
                "buyer_company_name='" + buyer_company_name + '\'' +
                ", buyer_company_type='" + buyer_company_type + '\'' +
                ", buyer_company_identification_type='" + buyer_company_identification_type + '\'' +
                ", buyer_company_identification_number='" + buyer_company_identification_number + '\'' +
                ", buyer_company_email='" + buyer_company_email + '\'' +
                ", buyer_company_email_password='" + buyer_company_email_password + '\'' +
                ", confirmed_password='" + confirmed_password + '\'' +
                '}';
    }
}
