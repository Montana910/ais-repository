package com.lizz.model.forms;

public class SellerFormInputs {

    private String seller_company_name;
    private String seller_company_type;
    private String seller_company_identification_type;
    private String seller_company_identification_number;
    private String seller_company_email;
    private String seller_company_email_password;
    private String confirmed_password;

    public String getSeller_company_name() {
        return seller_company_name;
    }

    public String getSeller_company_type() {
        return seller_company_type;
    }

    public String getSeller_company_identification_type() {
        return seller_company_identification_type;
    }

    public String getSeller_company_identification_number() {
        return seller_company_identification_number;
    }

    public String getSeller_company_email() {
        return seller_company_email;
    }

    public String getSeller_company_email_password() {
        return seller_company_email_password;
    }

    public String getConfirmed_password() {
        return confirmed_password;
    }

    public void setSeller_company_name(String seller_company_name) {
        this.seller_company_name = seller_company_name;
    }

    public void setSeller_company_type(String seller_company_type) {
        this.seller_company_type = seller_company_type;
    }

    public void setSeller_company_identification_type(String seller_company_identification_type) {
        this.seller_company_identification_type = seller_company_identification_type;
    }

    public void setSeller_company_identification_number(String seller_company_identification_number) {
        this.seller_company_identification_number = seller_company_identification_number;
    }

    public void setSeller_company_email(String seller_company_email) {
        this.seller_company_email = seller_company_email;
    }

    public void setSeller_company_email_password(String seller_company_email_password) {
        this.seller_company_email_password = seller_company_email_password;
    }

    public void setConfirmed_password(String confirmed_password) {
        this.confirmed_password = confirmed_password;
    }

    @Override
    public String toString() {
        return "SellerFormInputs{" +
                "seller_company_name='" + seller_company_name + '\'' +
                ", seller_company_type='" + seller_company_type + '\'' +
                ", seller_company_identification_type='" + seller_company_identification_type + '\'' +
                ", seller_company_identification_number='" + seller_company_identification_number + '\'' +
                ", seller_company_email='" + seller_company_email + '\'' +
                ", seller_company_email_password='" + seller_company_email_password + '\'' +
                ", confirmed_password='" + confirmed_password + '\'' +
                '}';
    }
}
