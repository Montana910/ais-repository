package com.lizz.service.serviceprovider;


import com.lizz.model.ServiceProvider;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ServiceProviderServiceImpl implements ServiceProviderService {


    @Override
    public void add(ServiceProvider serviceProvider) {

    }

    @Override
    public List<ServiceProvider> get() {
        return null;
    }

    @Override
    public ServiceProvider getById(Long id) {
        return null;
    }

    @Override
    public void update(ServiceProvider serviceProvider) {

    }

    @Override
    public void delete(Long id) {

    }
}
