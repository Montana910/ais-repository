package com.lizz.service.serviceprovider;


import com.lizz.model.ServiceProvider;
import java.util.List;

public interface ServiceProviderService {

    public void add (ServiceProvider serviceProvider);

    public List<ServiceProvider> get ();

    public ServiceProvider getById (Long id);

    public void update(ServiceProvider serviceProvider);

    public void delete (Long id);
}
