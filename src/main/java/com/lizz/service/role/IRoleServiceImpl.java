package com.lizz.service.role;

import com.lizz.model.Role;
import com.lizz.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IRoleServiceImpl implements IRoleService {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public void add(Role role) {
        roleRepository.save(role);
    }

    @Override
    public Iterable<Role> get() {
        return null;
    }

    @Override
    public Role getById(Long id) {
        return null;
    }

    @Override
    public void update(Role role) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public Role findByUserName(String name) {
        return null;
    }
}
