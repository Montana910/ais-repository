package com.lizz.service.role;

import com.lizz.model.Bid;
import com.lizz.model.Role;

public interface IRoleService {

    public void add (Role role);

    public Iterable<Role>get ();

    public Role getById (Long id);

    public void update(Role role);

    public void delete (Long id);

    public Role findByUserName (String name);
}
