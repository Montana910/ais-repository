package com.lizz.service.bid;

import com.lizz.model.Bid;

import java.util.List;

public interface BidService {

    public void add (Bid bid);

    public List<Bid> get ();

    public Bid getById (Long id);

    public void update(Bid bid);

    public void delete (Long id);

}
