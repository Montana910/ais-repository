package com.lizz.service.planner;

import com.lizz.model.Planner;

import java.util.List;

public interface PlannerService {

    public void add (Planner planner);

    public List<Planner> get ();

    public Planner getById (Long id);

    public void update(Planner planner);

    public void delete (Long id);

    public Planner findByEmail (String email) throws Exception;

    public Planner findByUserName (String firstName, String lastName, String email) throws Exception;


    public void addCustom (Planner planner);

    public List<Planner> getCustom ();

    public Planner getByIdCustom (Long id);

    public void updateCustom(Planner planner);

    public void deleteCustom (Long id);

}
