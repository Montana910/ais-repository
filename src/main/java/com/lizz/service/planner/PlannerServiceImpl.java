package com.lizz.service.planner;

import com.lizz.model.Planner;
import com.lizz.repository.PlannerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class PlannerServiceImpl implements PlannerService {

    @Autowired
    PlannerRepository plannerRepository;


    @Override
    public void add(Planner planner) {

        plannerRepository.save(planner);
    }

    @Override
    public List<Planner> get() {
        return null;
    }

    @Override
    public Planner getById(Long id) {
        return null;
    }

    @Override
    public void update(Planner planner) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public Planner findByEmail(String email) throws Exception {
        return null;
    }

    @Override
    public Planner findByUserName(String firstName, String lastName, String email) throws Exception {
        return null;
    }

    @Override
    public void addCustom(Planner planner) {

    }

    @Override
    public List<Planner> getCustom() {
        return null;
    }

    @Override
    public Planner getByIdCustom(Long id) {
        return null;
    }

    @Override
    public void updateCustom(Planner planner) {

    }

    @Override
    public void deleteCustom(Long id) {

    }
}
