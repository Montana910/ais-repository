package com.lizz.service.user;

import com.lizz.model.User;

import java.util.List;

public interface IUserService {

    public void add (User user);

    public User find (Long id);

    public List<User> getAll ();

    public void update (User user);

    public void delete (Long id);

    public User findByUserName (String userName);

    public void saveCustom (User user);

    public User findByEmail (String email) throws Exception;



}
