package com.lizz.service.user;

import com.lizz.model.User;
import com.lizz.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public void add(User user) {
        userRepository.save(user);
    }

    @Override
    public User find(Long id) {
        return null;
    }

    @Override
    public List<User> getAll() {
        return null;
    }

    @Override
    public void update(User user) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public User findByUserName(String userName) {
        return null;
    }

    @Override
    public void saveCustom(User user) {

    }

    @Override
    public User findByEmail(String email) throws Exception {
        return null;
    }
}
