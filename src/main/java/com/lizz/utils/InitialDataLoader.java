package com.lizz.utils;


import com.lizz.service.role.IRoleService;
import com.lizz.service.user.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class InitialDataLoader  {

    boolean alreadySetup = false;

    @Autowired
    private IUserService iUserService;

    @Autowired
    private IRoleService iRoleService;

//    @Autowired
//    private IPrivilegeService privilegeService;

    @Autowired
    private PasswordEncoder passwordEncoder;

}
