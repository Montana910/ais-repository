package com.lizz.utils;

import com.lizz.utils.UserType;

public class RegistrationUIInput {

    //General Inputs
    private UserType userType;
    private String firstName;
    private String surname;
    private String email;
    private String userName;
    private String password;

    //Planner Details
    private String pIndustry;
    private String pMrkSector;
    private String pCompany;
    private String pPosition;
    private String pWorkEmail;
    private String pWorkAddress;
    private String pWorkTell;
    private String pWorkContact;

    //service provider details
    private String spIndustry;
    private String spSector;
    private String spCompanyName;
    private String spWebSite;
    private String spEmail;
    private String spAddress;
    private String spTell;
    private String spContact;


}
