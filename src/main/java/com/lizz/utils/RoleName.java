package com.lizz.utils;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_PLANNER
}
