package com.lizz.utils;

import java.io.Serializable;

public class BidContainer implements Serializable {
    private Long id;
    private String bidTitle;
    private String bidDescription;
    private String bidOpenDate;
    private String bidCloseDate;
    private int bidHits;

    public BidContainer(Long id, String bidTitle, String bidDescription, String bidOpenDate, String bidCloseDate, int bidHits) {
        this.id = id;
        this.bidTitle = bidTitle;
        this.bidDescription = bidDescription;
        this.bidOpenDate = bidOpenDate;
        this.bidCloseDate = bidCloseDate;
        this.bidHits = bidHits;
    }


    public BidContainer() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBidTitle() {
        return bidTitle;
    }

    public void setBidTitle(String bidTitle) {
        this.bidTitle = bidTitle;
    }

    public String getBidDescription() {
        return bidDescription;
    }

    public void setBidDescription(String bidDescription) {
        this.bidDescription = bidDescription;
    }

    public String getBidOpenDate() {
        return bidOpenDate;
    }

    public void setBidOpenDate(String bidOpenDate) {
        this.bidOpenDate = bidOpenDate;
    }

    public String getBidCloseDate() {
        return bidCloseDate;
    }

    public void setBidCloseDate(String bidCloseDate) {
        this.bidCloseDate = bidCloseDate;
    }

    public int getBidHits() {
        return bidHits;
    }

    public void setBidHits(int bidHits) {
        this.bidHits = bidHits;
    }
}
