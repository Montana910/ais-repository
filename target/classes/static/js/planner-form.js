function submitAdForm() {
    if (!validateInput())
        return;

    var ajaxUrl = "http://localhost:8080/postAd";

    $.post(ajaxUrl,
      {
        bidTitle: $('#bidTitle').val(),
        bidDescription: $('#bidDescription').val(),
        bidOpenDate:  $('#bidOpenDate').val(),
        bidCloseDate: $('#bidCloseDate').val()
      },
      function(data, status){
        Swal.fire({
          title: data,
          showClass: {
            popup: 'animated fadeInDown faster'
          },
          hideClass: {
            popup: 'animated fadeOutUp faster'
          }
        });
        //$('#exampleModal').hide();
      });


}

function validateInput (){
    if ($('#bidTitle').val() == "" || $('#bidTitle').val() == null || $('#bidTitle').val() === undefined ){
        Swal.fire({
          title: 'Error!',
          text: 'Please enter bid title',
          icon: 'warning',
          confirmButtonText: 'Cool'
        });
        return false;
    }else if ($('#bidDescription').val() == "" || $('#bidDescription').val() == null || $('#bidDescription').val() === undefined ){
        Swal.fire({
          title: 'Error!',
          text: 'Bid Description may not be empty',
          icon: 'warning',
          confirmButtonText: 'Cool'
        });
        return false;
    }else if ($('#bidOpenDate').val() == "" || $('#bidOpenDate').val() == null || $('#bidOpenDate').val() === undefined){
        Swal.fire({
          title: 'Error!',
          text: 'Bid open date may not be empty',
          icon: 'warning',
          confirmButtonText: 'Cool'
        });
        return false;
    }else if ($('#bidCloseDate').val() == "" || $('#bidCloseDate').val() == null || $('#bidCloseDate').val() === undefined){
        Swal.fire({
          title: 'Error!',
          text: 'Bid close date may not be empty',
          icon: 'warning',
          confirmButtonText: 'Cool'
        });
        return false;
    }
    return (true);
}